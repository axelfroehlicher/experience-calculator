# Experience Calculator
Ce dépôt est le **rendu** du test de [Nexten.io](nexten.io)
![pipeline](https://img.shields.io/gitlab/pipeline/axelfroehlicher/experience-calculator)

## Installation

```bash
# Téléchargement 
git clone https://gitlab.com/axelfroehlicher/experience-calculator.git
cd experience-calculator

# installation des dépendences
npm install

# build typescript
npm run build
```

## Utilisation

###  Textuelle

- Le programme prend un chemin vers un fichier json en entrée et retourne l'expérience en nombre de jours.
```bash
# Usage: node . [options] <filename>

# lancement programme
node . example.json
node . -e example.json
```

- Quelques options sont disponibles:
```bash
node . -h
```

| Options        | Description      |
| ------|-----|
| -V, --version | Affiche la version du programme |
| -e, --easy | Affiche le résultat sous la forme suivante: 'x Year(s), x month(s) and x days' |
| -d, --debug | Montre les données de debug |
| -h, --help | Affiche les aides du programme |


###  Graphique

Je me suis amusé à dev une petite interface très simple en react pour faire des tests, elle permet de rajouter des expériences et de voir le résultat en temps réel pour faciliter les tests.

```bash
# backend
npm run serve

# frontend localhost:3000
cd client 
npm i && npm run start
```

![Test UI](https://gitlab.com/axelfroehlicher/experience-calculator/-/raw/master/test-ui.png)

## Tests
J'ai mis en place des tests unitaires lancés automatiquement à chaque commit avec la CI de gitlab. Il est aussi possible de lancer ces tests manuellement dans le projet avec la commande suivante:
```bash
npm run test
```

## Fonctionnement
Le but du programme est de calculer l'expérience globale d'une personne en inspectant l'ensemble de ces expériences, qu'elles soient terminées ou actuelles. 
Le plus important et le plus contraignant dans ce calcul, c'est de ne pas compter plusieurs fois le même jour si la personne a plusieurs travails en même temps. Il ne faut pas calculer le temps pour chaque expérience mais calculer les périodes où la personne était en activité.

Pour commencer je me suis occupé de prendre toutes les expériences et de les filtrer et modifier en fonction des informations reçues pour avoir une liste d'expériences correctement remplie sur laquelle je peux me baser pour effectuer le calcul. Plus d'informations sur les différents cas dans la prochaine section.

Avant le calul, je me permet de trier la liste des expériences avec la date de départ de la plus petite à la plus grande pour simplifier le calcul des périodes, c'est pour éviter de parcourir l'ensemble des expériences.

Ensuite je calcule les périodes sur lesquelles la personne était active. Et pour chaque période je vais compter le nombre de jours pour obtenir l'expérience globale.

- exemple:

```js
// Données brutes d'une personne, on peut y voir des incohérences qu'il faut corriger si on le peut.
[
    {
        "currentEmployed": true,
        "startDate": "2020-06-01",
        "endDate": null
    },
    {
        "currentEmployed": false,
        "startDate": "2020-06-01",
        "endDate": "2020-04-03"
    },
    {
        "currentEmployed": false,
        "startDate": null,
        "endDate": null
    },
    {
        "currentEmployed": true,
        "startDate": "2020-03-01",
        "endDate": "2020-05-03"
    }
]

// Données filtrées, modifiées et triées en essayant d'être le plus fidèle à la réalité avec les données de départ.
[
    {
        "currentEmployed": false, // changement vers false
        "startDate": "2020-03-01",
        "endDate": "2020-03-14"
    },
    {
        "currentEmployed": false,
        "startDate": "2020-04-03", // inversion date début et fin
        "endDate": "2020-06-01", // inversion date début et fin
    },
    {
        "currentEmployed": true,
        "startDate": "2020-05-27",
        "endDate": null
    }
]

// On calcule ensuite les périodes d'activités de la personne.
[
    {
        "startDate": "2020-03-01",
        "endDate": "2020-03-14"
    },
    {
        "startDate": "2020-04-03",
        "endDate": null // 2020/06/22
    }
]

// Et enfin on calcule la durée de chaque période et on retourne la somme totale.

// 15 + 81 = 96 **jours d'activités**

```


## Gestion des cas

La difficulté de l'exercice c'est de bien gérer les données que l'on reçoit pour effectuer un calcul d'expérience le plus proche de la réalité malgré les erreurs de remplissage. Comme on l'a vu précédemment, le programme effectue des changements dans certains cas et retire des expériences dans d'autres. Ci-dessous l'ensemble des cas possibles.

### Cas basique

- Cas basic d'une expérience en cours, dans ce cas, nous avons bien qu'une date de début, mais il se peut que l'expérience ne soit pas flaguée comment une expérience actuelle malgré l'absence d'une date de fin. D'après moi il est plus probable qu'une personne oublie d'indiquer que c'est une expérience actuelle que de remplir une date. Dans ce cas on forcera cette expérience comme étant actuelle.
```js
{
    "currentEmployed": true | false,
    "startDate": "2020-01-01",
    "endDate": null
}
// => 
{
    "currentEmployed": true, // on force true
    "startDate": "2020-01-01",
    "endDate": null
}
```

- Cas basic d'une expérience terminée, ici on a une expérience avec une date de début et une date de fin, on peut estimer que même si l'expérience est flaguée comme actuelle, elle ne l'est pas, car d'après moi il est plus probable qu'une personne se trompe en cochant une case qu'en indiquant une date de fin précise. Dans ce cas on forcera cette expérience comme étant terminée.
```js
{
    "currentEmployed": true | false,
    "startDate": "2020-01-01",
    "endDate": "2020-01-31"
}
// =>
{
    "currentEmployed": false, // on force false
    "startDate": "2020-01-01",
    "endDate": "2020-01-31"
}
```

### Cas inversion de dates

- Cas d'une expérience en cours avec une seule date, ici nous avons une expérience en cours mais avec une date de fin et sans date de début, on peut estimer que l'utilisateur s'est trompé de champ, on forcera la date de fin comme étant la date de début si et seulement si l'experience est flaguée comme actuelle.
```js
{
    "currentEmployed": true,
    "startDate": null,
    "endDate": "2021-01-01"
}
// => 
{
    "currentEmployed": true,
    "startDate": "2021-01-01",
    "endDate": null
}
```

- Cas d'une expérience terminée, avec les dates de début après la date de fin, ici on estimera que l'utilisateur a inversé les dates, et nous pouvons déduire que ce n'est pas une expérience actuelle, un traitement sera appliqué pour inverser les dates et confirmer que c'est une expérience passée.
```js
{
    "currentEmployed": true | false,
    "startDate": "2020-01-31",
    "endDate": "2020-01-01"
}
// =>
{
    "currentEmployed": false,
    "startDate": "2020-01-01",
    "endDate": "2020-01-31"
}
```

### Cas non condidérés

Cas sans dates, ces expériences ne seront pas prises en compte pour le calcul, car nous n'avons pas assez d'informations pour comprendre l'expérience de la personne.

- Ne pouvant rien déduire de l'expérience ci-dessous, elle ne sera pas utilisée pour le calcul.
```js
{
    "currentEmployed": true | false,
    "startDate": null,
    "endDate": null
}
```

- Cette expérience a seulement une date de fin et ce n'est pas une expérience actuelle, nous n'avons pas assez d'informations pour travailler avec cette expérience ou pour en déduire quelque chose, elle ne sera pas non plus utilisée pour le calcul.
```js
{
    "currentEmployed": false,
    "startDate": null,
    "endDate": "2020-04-01"
}
```
