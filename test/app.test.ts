import * as Calculator from "../src/lib/Calculator";
import moment = require("moment");
const fs = require("fs");

test("Basic test with 1 experience", () => {
  const data = JSON.parse(fs.readFileSync("examples/1-test-1-exp.json"));
  expect(Calculator.calculate(data)).toBe(10);
});

test("Basic test with 2 experiences", () => {
  const data = JSON.parse(fs.readFileSync("examples/2-test-2-exp.json"));
  expect(Calculator.calculate(data)).toBe(15);
});

test("Basic test with 2 simultaneously experiences", () => {
  const data = JSON.parse(fs.readFileSync("examples/3-test-2-sim-exp.json"));
  expect(Calculator.calculate(data)).toBe(20);
});

test("Basic test with inversed dates ", () => {
  const data = JSON.parse(fs.readFileSync("examples/4-test-inv-dates.json"));
  expect(Calculator.calculate(data)).toBe(11);
});

test("Basic test with 1 current exp with invalid dates ", () => {
  const data = JSON.parse(fs.readFileSync("examples/5-test-invalid-dates.json"));
  const expected: number = -moment(data[0].startDate).diff(moment(), "days") + 1;
  expect(Calculator.calculate(data)).toBe(expected);
});

test("Basic test with 1 current experience", () => {
  const data = JSON.parse(fs.readFileSync("examples/6-test-current-exp.json"));
  const expected = -moment(data[0].startDate).diff(moment(), "days") + 1;
  expect(Calculator.calculate(data)).toBe(expected);
});

test("Basic test with not started experience", () => {
  const data = JSON.parse(fs.readFileSync("examples/7-test-not-started-exp.json"));
  expect(Calculator.calculate(data)).toBe(0);
});

test("Basic test invalid current experience", () => {
  const data = JSON.parse(fs.readFileSync("examples/8-test-inv-current-exp.json"));
  const expected = -moment(data[0].startDate).diff(moment(), "days") + 1;
  expect(Calculator.calculate(data)).toBe(expected);
});

test("Basic test invalid current experience 2", () => {
  const data = JSON.parse(fs.readFileSync("examples/9-test-inv-current-exp-2.json"));
  expect(Calculator.calculate(data)).toBe(0);
});

test("Basic test finished current experience", () => {
  const data = JSON.parse(fs.readFileSync("examples/10-test-1-finished-current-exp.json"));
  expect(Calculator.calculate(data)).toBe(31);
});

test("Basic test null experience", () => {
  const data = JSON.parse(fs.readFileSync("examples/11-test-null-exp.json"));
  expect(Calculator.calculate(data)).toBe(0);
});

test("Basic test null current experience", () => {
  const data = JSON.parse(fs.readFileSync("examples/12-test-null-current-exp.json"));
  expect(Calculator.calculate(data)).toBe(0);
});

test("Basic test null current experience", () => {
  const data = JSON.parse(fs.readFileSync("examples/13-test-inv-current-dates.json"));
  expect(Calculator.calculate(data)).toBe(11);
});
