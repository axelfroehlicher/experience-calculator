import * as Calculator from "./lib/Calculator";
import { program } from "commander";
const fs = require("fs");

// usage and options management
let dataFile: string;
program
    .version("1.0.0")
    .description("Experience calculator for nexten.io")
    .option("-e, --easy", "Show the result as follows 'x Year(s), x month(s) and x days'")
    .option("-d, --debug", "Show debug data")
    .arguments("<filename>")
    .action((filename) => {
        dataFile = filename;
    })
    .parse(process.argv);

// argument check
let data: any[];
try {
    data = JSON.parse(fs.readFileSync(dataFile));
} catch (error) {
    console.log("error: No such file");
    process.exit(2);
}

// execution of the calculation
const counter = Calculator.calculate(data, program.debug);

// return
if (program.easy) {
    const years = Math.trunc(counter / 365);
    const months = Math.trunc((counter - years * 365) / 31);
    const days = counter - years * 365 - months * 31;
    console.log("≃ " + years + " YEAR(S), " + months + " MONTH(S) and " + days % 365 + " DAY(S)");
} else {
    console.log(counter);
}
