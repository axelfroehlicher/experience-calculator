import moment = require("moment");

export const calculate = (data: any[], debug: boolean = false): number => {

    if (debug) { console.log(data); }

    const tab = data.filter((element) => {

        // Init: moment date
        element.startDate = moment(element.startDate, "YYYY-MM-DD");
        element.endDate = moment(element.endDate, "YYYY-MM-DD");

        // filter to remove invalid experiences
        if ((!element.endDate.isValid() && !element.startDate.isValid())
            || (!element.startDate.isValid() && !element.currentlyEmployed)) {
                return false;
            }

        // modification for better understanding
        if (!element.endDate.isValid() && !element.currentlyEmployed) {
            element.currentlyEmployed = true;
        } else if (element.endDate.isBefore(element.startDate)
                && element.endDate.isValid()
                && element.startDate.isValid()) {
            const tmp = element.endDate;
            element.endDate = element.startDate;
            element.startDate = tmp;
            element.currentlyEmployed = true;
        } else if (!element.startDate.isValid() && element.currentlyEmployed) {
            const tmp = element.endDate;
            element.endDate = element.startDate;
            element.startDate = tmp;
        } else if (element.startDate.isBefore(element.endDate) && element.currentlyEmployed
                && element.endDate.isValid() && element.startDate.isValid()) {
            element.currentlyEmployed = false;
        }

        // filter and update on experience not completed
        if (element.startDate.isAfter(moment().format("YYYY-MM-DD"))) {
            return false;
        } else if (element.endDate.isAfter(moment().format("YYYY-MM-DD"))) {
            element.endDate = moment(null);
        }

        return true;

    }).sort((a, b) => (a.startDate.isBefore(b.startDate)) ? -1 : 1 );

    if (debug) { console.log(tab); }

    // Declaration of the activity table
    const arrayRange: any[] = [];
    tab.forEach((current) => {

        const index = arrayRange.length - 1;
        const previousExperience  = arrayRange[index];

        // Initialisation of the activity table
        if (arrayRange.length === 0) {

            arrayRange.push({
                endDate: current.endDate,
                startDate: current.startDate
            });

        } else {

            if (current.startDate.isAfter(previousExperience.endDate) && previousExperience.endDate.isValid()) {

                arrayRange.push({
                    endDate: current.endDate,
                    startDate: current.startDate
                });

            } else {

                if (previousExperience.endDate.isValid()) {

                    if (!current.endDate.isValid()
                        || (previousExperience.endDate.isBefore(current.endDate)
                        || previousExperience.endDate.isSame(current.endDate))) {
                        previousExperience.endDate = current.endDate;
                    }

                }
            }

        }

    });

    if (debug) { console.log(arrayRange); }

    let sum = 0;
    arrayRange.forEach( (element) => {

        // Initialization with today's date
        const date2 = (element.endDate == null) ? new Date(moment().format("YYYY-MM-DD")) : element.endDate;
        if (!element.endDate.isValid()) {
            element.endDate = moment().format("YYYY-MM-DD");
        }

        // sum of working days
        sum += -element.startDate.diff(element.endDate, "days") + 1;

    });

    return sum;

};
