import * as Calculator from "./lib/Calculator";
const express = require("express");
const http = require("http");
const logger = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");

// init
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.set("port", process.env.PORT || 8000);
app.use(logger("dev"));

// routes
app.post("/calculate", (req, res) => {
    const counter = Calculator.calculate(req.body, false);
    res.send({ counter }, 200);
});

// run
app.listen(app.get("port"), () => {
    console.log("Example app listening on port 8000!");
});
