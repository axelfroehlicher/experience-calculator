import React from "react";
import { Button, Card, Col } from 'react-bootstrap';

export default function Experience(props) {

    function handleRemoveClick(){
        props.remove(props.index);
    }

    function handleEditClick(){
        props.edit(props.index);
    }

    return (
        <Col md={4}>
            <Card bg={'light'} className={'m-1'}>
                <Card.Header>Experience {props.index+1}</Card.Header>
                <Card.Body>
                    <ul>
                        <li>Current: {props.experience.currentlyEmployed?'yes':'no'}</li>
                        <li>Start date: {props.experience.startDate}</li>
                        <li>End date: {props.experience.endDate}</li>
                    </ul>
                    <Button variant="danger" onClick={handleRemoveClick}>
                        Remove
                    </Button> &nbsp;
                    <Button variant="info" onClick={handleEditClick}>
                        Edit
                    </Button>
                </Card.Body>
            </Card>
        </Col>
    );

}