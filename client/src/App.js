import React, {  useState, useEffect } from 'react';
import { Navbar, Container, Card, Form, Button, Row, Col } from 'react-bootstrap';
import Experience from './components/Experience';
import DatePicker from "react-datepicker";

// css
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import "react-datepicker/dist/react-datepicker.css";

const axios = require('axios').default;

function App() {

  const initTime = { 
    years: 0,
    months: 0,
    days: 0,
    counter: 0
  };

  const initForm = {
    startDate: null,
    endDate: null,
    currentlyEmployed: false
  }

  // data
  const [experiences, setExperiences] = useState([]);
  const [times, setTimes] = useState(initTime);

  // form
  const [form, setForm] = useState(initForm);

  function handleSubmit(e){
    e.preventDefault();

    setExperiences([
      ...experiences,
      {
        currentlyEmployed: form.currentlyEmployed,
        startDate: (form.startDate)?form.startDate.getFullYear()+"-"+(form.startDate.getMonth()+1)+"-"+form.startDate.getDate():null,
        endDate: (form.endDate)?form.endDate.getFullYear()+"-"+(form.endDate.getMonth()+1)+"-"+form.endDate.getDate():null
      }
    ]);

    initFormData(null, null, null);
  }

  useEffect(() => {
    axios.post('http://localhost:8000/calculate', experiences)
    .then(({data}) => {
      const years = Math.trunc(data.counter / 365);
      const months = Math.trunc((data.counter - years * 365) / 31);
      const days = data.counter - years * 365 - months * 31;
      setTimes({
        years,
        months,
        days,
        counter: data.counter
      })
    }, (error) => {
      console.log(error);
    });
  },[experiences]);

  function handleRemoveClick(index){
    removeExperience(index);
  }

  function handleEditClick(index){
    initFormData(experiences[index].startDate, experiences[index].endDate, experiences[index].currentlyEmployed);
    removeExperience(index);
  }

  function removeExperience(index){
    experiences.splice(index,1);
    setExperiences([
      ...experiences
    ]);
  }

  function initFormData(startDate, endDate, current){
    setForm({
      endDate: endDate?new Date(endDate):null,
      startDate: startDate?new Date(startDate):null,
      currentlyEmployed: current
    })
  }

  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#">Experience Calculator</Navbar.Brand>
      </Navbar>
      <br />
      <Container>
        <Row>
          <Col>
            <Card>
              <Card.Header>Add a new experience</Card.Header>
              <Card.Body>

                <Form onSubmit={handleSubmit}>
                  <Form.Group controlId="formBasicStartDate">
                    <Form.Label>Start date </Form.Label> &nbsp;
                    <DatePicker
                      dateFormat={'yyyy-MM-dd'}
                      selected={form.startDate}
                      onChange={(v) => {setForm({ ...form, startDate: v})}}
                    />
                  </Form.Group>

                  <Form.Group controlId="formBasicEndDate">
                    <Form.Label>End date </Form.Label> &nbsp;
                    <DatePicker
                      dateFormat={'yyyy-MM-dd'}
                      selected={form.endDate}
                      onChange={(v) => {setForm({ ...form, endDate: v})}}
                    />
                  </Form.Group>

                  <Form.Group controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" checked={form.currentlyEmployed} label="Current experience" onChange={(e) => { setForm({ ...form, currentlyEmployed: e.target.checked}) }} />
                  </Form.Group>

                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Form>
                
              </Card.Body>
            </Card>
          </Col>
          <Col className={'text-center mt-2 mb-2'}>
            <p>This application is used to test experience calculator developed for <a href='https://Nexten.io' rel="noopener noreferrer" target='_blank'>Nexten.io</a>.</p>
            <h2 className={''}>{times.counter} day(s)</h2>
            <h4 className={''}>≃ {times.years} YEAR(S), {times.months} MONTH(S) and {times.days} DAY(S)</h4>
          </Col>
        </Row>
        <hr/>
        <Row>
          {(!experiences.length) ? (
              <p>Please add a new experience</p>
            ) : (
              experiences.map((exp, index) => {
                return <Experience key={index} experience={exp} index={index} edit={handleEditClick.bind(this)} remove={handleRemoveClick.bind(this)}/>
              })
          )}
        </Row>
      </Container>

    </>
  );
}

export default App;
